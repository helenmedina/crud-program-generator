﻿using System;
using System.Collections.Generic;

namespace AppDB
{
	public class <%= table.getName.capitalize %>Model
	{
		public string TableName { get; set;}

		//table fields
<%- columns.each { |column|
	name = column["name"] -%>
		private string _<%= name %>;
<%}%>	
		//check if the field has been modified
<%- columns.each { |column|
	name = column["name"] -%>
		public bool <%= name.capitalize %>Updated { get; set;}		
<%}%>	

		//contains fields and the filters for the WHERE action
		public List<string> Fields { get; set;}
		public List<string> FilteredFields { get; set;}

		public <%= table.getName.capitalize %>Model ()
		{
			Fields = new List<string>();
			FilteredFields = new List<string>();
			TableName = "<%= table.getName %>";
<%- columns.each { |column|
	name = column["name"] -%>
			set<%= name.capitalize %>Field("<%= name %>");
<%}%>	
		}
		//get setters
<%- columns.each { |column|
	name = column["name"] -%>
		public void set<%= name.capitalize %>Field(string <%= name %>)
		{
			this._<%= name %> = <%= name %>;
			<%= name.capitalize %>Updated = false;
			addField (this._<%= name %>);
		}
		public string get<%= name.capitalize %>Field()
		{
			return this._<%= name %>;
		}
<%}%>	
		public void setDirty(string field)
		{
		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			if (counter == 0)-%>
			if (field == this._<%= name %>)
				this.<%= name.capitalize %>Updated = true;
			<%-else -%>
			else if (field == this._<%= name %>)
				this.<%= name.capitalize %>Updated = true;
		<%-end 
		counter = counter +1} -%>
		}

		public bool isDirty(string field)
		{
		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			if (counter == 0)-%>
			if (field == this._<%= name %>)
				return (this.<%= name.capitalize %>Updated == true);
			<%-else -%>
			else if (field == this._<%= name %>)
				return (this.<%= name.capitalize %>Updated == true);
		<%-end 
		counter = counter +1} -%>
			else 
				return false;
		}

		public void addFilter(string field)
		{
			FilteredFields.Add (field);
		}

		public void clearFilter()
		{
			FilteredFields.Clear();
		}
			
		private void addField(string field)
		{
			Fields.Add (field);
		}


	}
}

