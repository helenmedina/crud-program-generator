﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace AppDB
{
	public partial class <%= table.getName.capitalize %>Logic : IEntity
	{
		private string _query;
		private string _filter;
		private string _auxBindingValues;

<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
		private <%= type %> _<%= name %>;
<%}%>

<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
		private <%= type %> Filter<%= name.capitalize %>;
<%}%>

		private <%= table.getName.capitalize %>Model _model;
					
		public <%= table.getName.capitalize %>Logic ()
		{
			_model = new <%= table.getName.capitalize %>Model();

<%- columns.each { |column|
	name = column["name"]
	type = column["type"]  
		if (type == "int")-%>
			_<%= name %> = 0;
		<%-elsif (type == "double") -%>
			_<%= name %> = 0.0;
		<%-elsif (type == "string") -%>
			_<%= name %> = "";
		<%-elsif (type == "date") -%>
			_<%= name %> = "";
<%-end } -%>

<%- columns.each { |column|
	name = column["name"] 
	type = column["type"] 
		if (type == "int")-%>
			Filter<%= name.capitalize %> = 0;
		<%-elsif (type == "double") -%>
			Filter<%= name.capitalize %> = 0.0;
		<%-elsif (type == "string") -%>
			Filter<%= name.capitalize %> = "";
		<%-elsif (type == "date") -%>
			Filter<%= name.capitalize %> = "";
<%-end } -%>

			_auxBindingValues = "";
			_filter = "";
			_query = "";
	
		}

		public void insert (MySqlConnection conn)
		{
			MySqlCommand cmd = new MySqlCommand ();
			cmd.Connection = conn;
			_query = "";
			_query = "INSERT INTO " + getTable() + " (";
			_query += getFieldsToInsert();
			_query += " ) VALUES ( " + getValuesToBind() + ")";

			executeQuery (cmd);
		}

		public void delete (MySqlConnection conn)
		{
			MySqlCommand cmd = new MySqlCommand ();
			cmd.Connection = conn;
			_query = "";
			_query = "DELETE FROM " + getTable() + " WHERE " + getFilter();

			executeQuery (cmd);
		}

		public void update (MySqlConnection conn)
		{
			MySqlCommand cmd = new MySqlCommand ();
			cmd.Connection = conn;
			_query = "";
			_query = "UPDATE " + getTable() + " SET " + getColumnsUpdated() + " WHERE " + getFilter();

			executeQuery (cmd);
		}

		public IEntity find(MySqlConnection conn)
		{
			MySqlCommand cmd = new MySqlCommand ();
			cmd.Connection = conn;
			_query = "";
			_query = "SELECT * FROM " + getTable() + " WHERE " + getFilter();
		
			cmd.CommandText = _query;
			cmd.Prepare ();
			bindValues(cmd);
			MySqlDataReader reader = cmd.ExecuteReader();
			if (reader.Read())
			{
				foreach (var field in _model.Fields) {
					updateValue(field, reader);
				}

			}
			reader.Close();

			_model.clearFilter();
			return this;
		}

		public List<string> getFields()
		{
			return _model.Fields;
		}
		public List<string> getFilterFields()
		{
			return _model.FilteredFields;
		}

		public void showDetails()
		{
			Console.Clear ();
			foreach (var field in _model.Fields) {

		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			if (counter == 0)-%>
				if (field == _model.get<%= name.capitalize %>Field ()) {
					Console.Write (field);
					Console.Write (": ");
					Console.WriteLine (this.get<%= name.capitalize %> ());
				}
			<%-else -%>
				else if (field == _model.get<%= name.capitalize %>Field ()) {
					Console.Write (field);
					Console.Write (": ");
					Console.WriteLine (this.get<%= name.capitalize %> ());
				}
		<%-end 
		counter = counter +1} -%>

			}
		}

		public void setField (string field, string valueField)
		{

		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			type = column["type"] 
			if (counter != 0)-%>			else<% else %>			<% end %> if (field == _model.get<%= name.capitalize %>Field ()) {
				<%-if (type == "int")-%>
				<%= type%> <%= name%> = Convert.ToInt32 (valueField);
				<%-elsif (type == "double") -%>
				<%= type%> <%= name%> = Convert.ToDouble (valueField);
				<%-elsif (type == "string") -%>
				<%= type%> <%= name%> = Convert.ToString (valueField);
				<%-elsif (type == "date") -%>
				<%= type%> <%= name%> = Convert.ToString (valueField);
				<%-end -%>
				this.set<%= name.capitalize %> (<%= name%>);
			} 
		<%- counter = counter +1} -%>
			
		}

		public void setFilterFields (string field, string valueField)
		{

		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			type = column["type"] 
			if (counter != 0)-%>			else<% else %>			<% end %> if (field == _model.get<%= name.capitalize %>Field ()) {
				<%-if (type == "int")-%>
				<%= type%> <%= name%> = Convert.ToInt32 (valueField);				
				<%-elsif (type == "double") -%>
				<%= type%> <%= name%> = Convert.ToDouble (valueField);
				<%-elsif (type == "string") -%>
				<%= type%> <%= name%> = Convert.ToString (valueField);
				<%-elsif (type == "date") -%>
				<%= type%> <%= name%> = Convert.ToString (valueField);				
				<%-end -%>
				Filter<%= name.capitalize %> = <%= name%>;
				
			} 
		<%- counter = counter +1} -%>

			_model.addFilter (field);
				
		}

		private void executeQuery (MySqlCommand cmd)
		{
			cmd.CommandText = _query;
			cmd.Prepare ();
			bindValues(cmd);

<%- options.each { |option|
	type = option["type"] 
	optionSelected = option["optionSelected"]
		if (type == "resultQueryExecution") -%>
	<%- if (optionSelected == "resultQuery") -%>
			int rowCount = cmd.ExecuteNonQuery();
			if (rowCount > 0)
				Console.Write ("Success.");
			else
				Console.Write ("Failed.");
	<%- elsif (optionSelected == "numberRowsExecuted") -%>
			int rowCount = cmd.ExecuteNonQuery();
			Console.Write (rowCount);
			Console.Write (" rows.");
	<%- else -%>
			cmd.ExecuteNonQuery();
<%-end -%>
<%-end } -%>

			_model.clearFilter();		
		}

		private void bindValues(MySqlCommand cmd)
		{

			foreach (var field in _model.Fields)
			{
				if (_model.isDirty(field))
					getValue(field, cmd);
		
			}

			bindFilterValues(cmd);

		}

		private void bindFilterValues(MySqlCommand cmd)
		{

			foreach (var field in _model.FilteredFields)
				getFilterValue(field, cmd);

		}

		private string getFilter()
		{
			int i = 0;
			foreach (var field in _model.FilteredFields) {
				if (i == 0)
				{
					_filter = string.Concat(_filter, field);  
					_filter = string.Concat(_filter, " = @Filter");  
					_filter = string.Concat(_filter, field); 
				}
<%- options.each { |option|
	type = option["type"] 
	optionSelected = option["optionSelected"]
		if (type == "conjunction" and optionSelected != "nil")-%>
				else
				{
					_filter = string.Concat(_filter, " <%= optionSelected.upcase %> ");  
					_filter = string.Concat(_filter, field); 
					_filter = string.Concat(_filter, " = @Filter");  
					_filter = string.Concat(_filter, field); 
				}

<%-end } -%>

				i++;

			}

			return _filter;
		}

		public string getTable()
		{
			return _model.TableName;
		}
		//get setters
<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
		public void set<%= name.capitalize %>(<%= type %> <%= name.capitalize %>)
		{
			_model.setDirty ("<%= name %>");
			this._<%= name %> = <%= name.capitalize %>;

		}
		public <%= type %> get<%= name.capitalize %>()
		{
			return this._<%= name %>;
		}
<%}%>

		//private


		private string getFieldsToInsert()
		{

			string strFields = "";
			int counter = 0;

			foreach (var field in _model.Fields) {
				
				if (_model.isDirty(field))
				{
					if(counter != 0)
					{
						strFields = string.Concat(strFields, ", ");  
						strFields = string.Concat(strFields, field);
						_auxBindingValues = string.Concat(_auxBindingValues, ", ");
						_auxBindingValues = string.Concat(_auxBindingValues, "@");
						_auxBindingValues = string.Concat(_auxBindingValues, field);
					}
					else
					{
						strFields = string.Concat(strFields, field); 
						_auxBindingValues = string.Concat(_auxBindingValues, "@");
						_auxBindingValues = string.Concat(_auxBindingValues, field);
					}
					counter++;
				}
			}


			return strFields;
		}

		private string getValuesToBind()
		{
			return _auxBindingValues;
		}


		private void getValue(string strField, MySqlCommand cmd )
		{
			string bind = string.Concat("@", strField);

		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			if (counter == 0)-%>
			if (strField == _model.get<%= name.capitalize %>Field())
				cmd.Parameters.AddWithValue(bind, this.get<%= name.capitalize %> ());
			<%-else -%>
			else if (strField == _model.get<%= name.capitalize %>Field())
				cmd.Parameters.AddWithValue(bind, this.get<%= name.capitalize %> ());
		<%-end 
		counter = counter +1} -%>


		}

		private void getFilterValue(string strField, MySqlCommand cmd )
		{
			string bind = string.Concat("@Filter", strField);

		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			if (counter == 0)-%>
			if (strField == _model.get<%= name.capitalize %>Field())
				cmd.Parameters.AddWithValue(bind, this.Filter<%= name.capitalize %>);
			<%-else -%>
			else if (strField == _model.get<%= name.capitalize %>Field())
				cmd.Parameters.AddWithValue(bind, this.Filter<%= name.capitalize %>);
		<%-end 
		counter = counter +1} -%>

		}

		private void updateValue(string strField, MySqlDataReader dataReader)
		{


		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			type = column["type"] 
			if (counter != 0)-%>			else<% else %>			<% end %> if (strField == _model.get<%= name.capitalize %>Field()) {
				<%-if (type == "int")-%>
				this.set<%= name.capitalize %>((int)dataReader[strField]);				
				<%-elsif (type == "double") -%>
				this.set<%= name.capitalize %>((double)dataReader[strField]);	
				<%-elsif (type == "string") -%>
				this.set<%= name.capitalize %>((string)dataReader[strField]);
				<%-elsif (type == "date") -%>
				this.set<%= name.capitalize %>((string)dataReader[strField]);			
				<%-end -%>							
			} 
		<%- counter = counter +1} -%>

		}


		private string getColumnsUpdated()
		{
			string strFields = "";
			int counter = 0;

			foreach (var field in _model.Fields) {

				if (_model.isDirty(field))
				{
					if(counter == 0)
					{
						strFields = string.Concat(strFields, field);  
						strFields = string.Concat(strFields, " = @");  
						strFields = string.Concat(strFields, field); 
					}
					else
					{
						strFields = string.Concat(strFields, ","); 
						strFields = string.Concat(strFields, field);  
						strFields = string.Concat(strFields, " = @");  
						strFields = string.Concat(strFields, field); 
					}
					counter++;
				}
			}
				
			return strFields;
		}

		//end class	
	}


}

