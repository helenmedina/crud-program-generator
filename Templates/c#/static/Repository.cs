﻿using System;

namespace AppDB
{
	public interface IRepository<T> where T: IEntity
	{
		void add( T entity );
		void delete( T entity );
		void update( T entity );
		T find(T entity);
		void setDatabaseContext(eDatabase db);
	}
}

