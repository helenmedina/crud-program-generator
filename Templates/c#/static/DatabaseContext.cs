﻿using System;
using MySql.Data.MySqlClient;

namespace AppDB
{
	public class DatabaseContext
	{
		private MySqlConnection connection = null;
		private string _server;
		private string _user;
		private string _password;
		private string _database;

		public DatabaseContext (string server, string user, string password, string db)
		{
			_server = server;
			_user = user;
			_password = password;
			_database = db;
			connect ();
		}

		public void addEntity(IEntity entity)
		{
			entity.insert(connection);
		}

		public void deleteEntity(IEntity entity)
		{
			entity.delete(connection);
		}

		public void updateEntity(IEntity entity)
		{
			entity.update(connection);
		}

		public void find(IEntity entity)
		{
			entity.find(connection);
		}

		void connect()
		{

			string connectionDetails = string.Format("Server={0}; database={1}; UID={2}; password={3}", _server, _database,_user, _password);
			connection = new MySqlConnection(connectionDetails);
			connection.Open();

		}

		void closeConnection()
		{
			connection.Close ();
		}
	}
}

