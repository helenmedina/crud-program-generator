﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace AppDB
{
	public interface IEntity
	{
		void insert (MySqlConnection conn);
		void delete (MySqlConnection conn);
		void update (MySqlConnection conn);
		string getTable();
		IEntity find(MySqlConnection conn);
		List<string> getFields();
		void setField (string field, string valueField);
		List<string> getFilterFields ();
		void setFilterFields (string field, string valueField);
		void showDetails ();

	}
}

