﻿using System;

namespace AppDB
{
	public class <%= name.capitalize %>Repository : IRepository<<%= name.capitalize %>Logic>
	{
		private DatabaseContext _<%= name %>Context;
		public <%= name.capitalize %>Repository ()
		{
			_<%= name %>Context = null;
		}

		public void setDatabaseContext(eDatabase db)
		{
			_<%= name %>Context =  new DatabaseContext(db.server, db.user, db.password, db.database);

		}
		public void add (<%= name.capitalize %>Logic entity)
		{
			_<%= name %>Context.addEntity(entity);

		}
		public void delete(<%= name.capitalize %>Logic entity)
		{
			_<%= name %>Context.deleteEntity(entity);
		}

		public void update (<%= name.capitalize %>Logic entity)
		{
			_<%= name %>Context.updateEntity(entity);
		}

		public <%= name.capitalize %>Logic find(<%= name.capitalize %>Logic entity)
		{
			_<%= name %>Context.find(entity);
			return entity;
		}
	}
}

