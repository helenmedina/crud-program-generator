﻿using System;

namespace AppDB
{
	public class Factory
	{
		public Factory ()
		{
		}

		public IEntity createEntity (Tables eTable)
		{
			IEntity ptr = null;
			switch(eTable) 
			{
			<%-names.each { |name| if (name != "nil")-%>			case Tables.<%= name.upcase %>:
				ptr = new <%= name.capitalize %>Logic ();
				break;
			<%- end -%>
			<%- } -%>
			}

			return ptr;

		}
	}
}

