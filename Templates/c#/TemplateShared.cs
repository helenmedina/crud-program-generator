﻿using System;

namespace AppDB
{

	public struct eDatabase
	{
		public string user;
		public string password;
		public string server;
		public string database;
	};

	public enum Tables
	{
		NONE = 0<%-names.each { |name| if (name != "nil")-%>,
		<%= name.upcase %><%- end -%>
<% } %>

	};

	public enum eActions
	{
		INSERT = 1,
		DELETE = 2,
		UPDATE = 3,
		SELECT = 4
	};
}

