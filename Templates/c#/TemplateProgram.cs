﻿using System;

namespace AppDB
{
	class MainClass
	{

		public static void executeAction<T>(IRepository<T> repository, eDatabase db, T entity, int iOption) where T: IEntity
		{
			repository.setDatabaseContext(db);

			switch((eActions)iOption)
			{
			case eActions.INSERT:
				repository.add (entity);
				break;
			case eActions.DELETE:
				repository.delete (entity);
				break;
			case eActions.SELECT:
				IEntity resultEntity = repository.find (entity);
				resultEntity.showDetails ();
				break;
			case eActions.UPDATE:
				repository.update (entity);
				break;

			}

		}

		private static void displayOptions (int iOption)
		{
			Console.Clear ();
			switch(iOption) {
			case 1:
				Console.WriteLine ("Set fields");
				break;
			case 2 : 
				Console.WriteLine ("Set filters");
				break;
			case 3 : 
				Console.WriteLine ("Set fields");

				break;
			case 4 : 
				Console.WriteLine ("Set filters");
				break;
			}
		}

		private static void fillFieldParameters (IEntity entity, bool bField, bool bIsInsert)
		{
			bool filterIsSelected = false;
			string name = entity.getTable();

			foreach (var field in entity.getFields()) {
				Console.WriteLine (field);
				var valueField = Console.ReadLine();
				//if (valueField == "q") 
				//	break;
				//if the user enter "none" -> this field won't be pplied on the filter or the update
				if (valueField != "none") {										
<%-	tables.each{ |table|
		tablename = table.getName
		options = table.getOptions
		options.each { |option|
			type = option["type"] 
			optionSelected = option["optionSelected"]
			if (type == "conjunction" and optionSelected == "nil")-%>
					if (name == "<%= tablename %>" && !bIsInsert)
					{
						if (filterIsSelected){
							Console.WriteLine ("Filter has been already selected. only one filter is allowed for the table <%= tablename.capitalize %>.");
							continue;	
						}
					}
	<%-end }-%> <%- } -%>

					filterIsSelected = true;

					if(bField)
						entity.setField (field, valueField);
					else
						entity.setFilterFields (field, valueField);
				}
				else
				{
					if(bIsInsert)
					{
<%-	tables.each{ |table|
		tablename = table.getName
		columns = table.getColumns -%>

							if (name == "<%= tablename %>")
							{
		<%-columns.each { |column|
			name = column["name"] 
			nullable = column["nullable"] -%>

				<%-	if (nullable == "N")-%>
								if ("<%= name %>" == field)
									throw new Exception("This field cannot be empty");	
	<% end }%> 							} 
					
<% } %>
					}
				}
			}
			
		}
			
		private static void fillFieldParameters (int iAction, IEntity entity)
		{
			bool bIsField = true;
			switch((eActions)iAction)
			{
			case eActions.INSERT:
				bIsField = true;
				fillFieldParameters (entity, bIsField, true);
				
				break;

			case eActions.DELETE:
				bIsField = false;
				fillFieldParameters (entity, bIsField, false);
				
				break;

			case eActions.SELECT:
				bIsField = false;
				fillFieldParameters (entity, bIsField, false);	
				break;

			case eActions.UPDATE:
				// set the values for the fields to be modified
				bIsField = true;
				fillFieldParameters (entity, bIsField, false);	
				//set the fields to use in the filter
				Console.WriteLine ("Set filters");
				Console.ReadLine();
				bIsField = false;
				fillFieldParameters (entity, bIsField, false);	
				
				break;

			}
				
		}

		private static void setAndExecuteAction (int iOption, Tables tableType, IEntity entity, eDatabase db)
		{
			displayOptions (iOption);

			Console.ReadLine();
			fillFieldParameters (iOption, entity);


<%-counter = 0
names.each { |name| 
	if (name != "nil")
	if (counter != 0)-%>			else <% else %>			<% end %>if (tableType == Tables.<%= name.upcase %>) {
		
				IRepository<<%= name.capitalize %>Logic> <%= name %>Repository = new <%= name.capitalize %>Repository();
				<%= name.capitalize %>Logic <%= name %> = entity as <%= name.capitalize %>Logic;
					
				if (<%= name %> != null) {
					executeAction(<%= name %>Repository, db, <%= name %>, iOption);
				}
		
			}
<%- end -%>
<%- counter = counter + 1} -%>		
								
		}
				

		private static eDatabase setUpDatabase (int iInput)
		{
			eDatabase db;
			db.user = "";
			db.server = "";
			db.password = "";
			db.database = "";

			if (iInput == 1) {
				//connect to the MYSQL database
				db.user = "root";
				db.password = "Qc62fTLa";
				db.server = "localhost";
				db.database = "school";

			} else if (iInput == 2) {
				//connect to the SQLITE database
			}else{
					throw new Exception("Only MSQL and sqlite databases are allowed. Please selet option 1 or 2.");
			}

			return db;
		}

		private static Tables getEnumTable (string tablename)
		{
			Tables table = Tables.NONE;
<%-counter = 0
names.each { |name| 
	if (name != "nil")
	if (counter != 0)-%>			else <% else %>			<% end %>if (tablename == "<%= name %>") {
				table = Tables.<%= name.upcase %>;
		
			}
<%- end -%>
<%- counter = counter + 1} -%>	

			return table;
		}

		private static IEntity createEntity (Tables tableType)
		{
			Factory factory = new Factory ();
			IEntity entity = factory.createEntity (tableType);

			return entity;
		}


		public static void Main (string[] args)
		{
			try {
				Console.WriteLine ("Select database:");

				Console.WriteLine ("1 - Database Mysql");
				Console.WriteLine ("2 - SQLite");

				int iInput = Convert.ToInt32 (Console.ReadLine());

			 	eDatabase db = setUpDatabase (iInput);

				Console.Clear ();
				Console.WriteLine ("Insert table name:");
				string tablename = Convert.ToString (Console.ReadLine ());
				Tables tableType = getEnumTable (tablename);

				IEntity entity = createEntity (tableType);
				if (entity == null)
				{
					string msg = String.Format("Entity {0} doesn't exist", tablename);
					throw new Exception(msg);
				}

				Console.Clear ();

				Console.WriteLine ("Menu options:");
				Console.WriteLine ("1 - Insert");
				Console.WriteLine ("2 - Delete");
				Console.WriteLine ("3 - Update");
				Console.WriteLine ("4 - Select");

				int iOption = Convert.ToInt32 (Console.ReadLine());
				setAndExecuteAction (iOption, tableType, entity, db);
			}
			catch( Exception e)
			{  
				Console.WriteLine(e.ToString());
			}

		}
	}
}
