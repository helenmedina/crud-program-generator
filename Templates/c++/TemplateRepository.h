#ifndef <%= name.upcase %>_REPOSITORY_H
#define <%= name.upcase %>_REPOSITORY_H
#include "Repository.h"

class <%= name.capitalize %>Logic;
class DatabaseContext;


class <%= name.capitalize %>Repository : public Repository<<%= name.capitalize %>Logic*>
{
public:
	<%= name.capitalize %>Repository();
	~<%= name.capitalize %>Repository();
	void add (<%= name.capitalize %>Logic*);
	void Delete (<%= name.capitalize %>Logic*);
	void update (<%= name.capitalize %>Logic*);
	<%= name.capitalize %>Logic* find(<%= name.capitalize %>Logic* entity);
	void setDatabaseContext(eDatabase db);

private:
	DatabaseContext *m_<%= name %>Context;

};

#endif //<%= name.upcase %>_REPOSITORY_H
