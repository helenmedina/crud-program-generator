#include "Factory.h"
#include "shared.h"
<%-names.each { |name| if (name != "nil")-%>
#include "<%= name.capitalize %>Logic.h"
<%- end }-%>
EntityBase* Factory::createEntity (Tables eTable)
{
	EntityBase *ptr = nullptr;
	switch(eTable) 
	{
			<%-names.each { |name| if (name != "nil")-%>			case Tables::<%= name.upcase %>:
				ptr = new <%= name.capitalize %>Logic ();
				break;
			<%- end -%>
			<%- } -%>		
	}

	return ptr;

}
