#ifndef SHARED_H
#define SHARED_H
#include <string>

enum Tables
{
	NONE = 0<%-names.each { |name| if (name != "nil")-%>,
	<%= name.upcase %><%- end -%>
<% } %>
};

enum eActions
{
	INSERT = 1,
	DELETE = 2,
	UPDATE = 3,
	SELECT = 4
};

struct eDatabase
{
	std::string user;
	std::string password;
	std::string server;
	std::string database;
};

#endif
