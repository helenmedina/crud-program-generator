#include "<%= name.capitalize %>Logic.h"
#include "DatabaseContext.h"
#include "<%= name.capitalize %>Repository.h"

<%= name.capitalize %>Repository::<%= name.capitalize %>Repository()
{
	m_<%= name%>Context = nullptr; 	
}

void <%= name.capitalize %>Repository::setDatabaseContext(eDatabase db)
{
	m_<%= name%>Context =  new DatabaseContext(db.server, db.user, db.password, db.database);
		
}
void <%= name.capitalize %>Repository::add (<%= name.capitalize %>Logic* entity)
{
	m_<%= name%>Context->addEntity(entity);

}
void <%= name.capitalize %>Repository::Delete (<%= name.capitalize %>Logic* entity)
{
	m_<%= name%>Context->deleteEntity(entity);
}

void <%= name.capitalize %>Repository::update (<%= name.capitalize %>Logic* entity)
{
	m_<%= name%>Context->updateEntity(entity);
}

<%= name.capitalize %>Logic* <%= name.capitalize %>Repository::find(<%= name.capitalize %>Logic* entity)
{
	m_<%= name%>Context->find(entity);
	return entity;
}

<%= name.capitalize %>Repository::~<%= name.capitalize %>Repository()
{
	delete m_<%= name%>Context;
}
