<%-names.each { |name| if (name != "nil")-%>
#include "<%= name.capitalize %>Logic.h"
#include "<%= name.capitalize %>Repository.h"
<%- end } -%>
#include "Factory.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <exception>


template<typename T> void executeAction(Repository<T> *repository, eDatabase db, EntityBase* entityBase, int iOption) 
{
	repository->setDatabaseContext(db);
	T entity= dynamic_cast<T>(entityBase);

	switch(iOption)
	{
	case eActions::INSERT:
		repository->add (entity);
		break;

	case eActions::DELETE:
		repository->Delete (entity);
		break;

	case eActions::UPDATE:
		repository->update (entity);
		break;

	case eActions::SELECT:
		EntityBase* resultEntity = repository->find (entity);
		resultEntity->showDetails ();
		break;

	}

}

void displayOptions (int iOption)
{
	std::system("clear");

	switch(iOption) {

	case 1:
		std::cout << "Set fields" << std::endl;
		break;

	case 2 : 
	std::cout << "Set filters" << std::endl;
		break;

	case 3 : 
		std::cout << "Set fields" << std::endl;
		break;

	case 4 : 
		std::cout << "Set filters" << std::endl;
		break;
	}
}

void fillFieldParams (EntityBase* entity, bool bField, bool bIsInsert)
{
	std::string sInput;
	std::vector<std::string> fields = entity->getFields();
	bool filterIsSelected = false;
	string name = entity->getTable();	
	
	for (unsigned int i = 0; i < fields.size(); i++)
	{
		std::string field = fields[i];
		std::cout << field << std::endl;
		getline (std::cin, sInput);

		//If the user press q, the 
		//if (sInput == "q") 
		//	break;

		//if the user enter "none" -> this field won't be pplied on the filter or the update
		if (sInput != "none") {
							
			if(bField)
				entity->setField (field, sInput);
			else
			{
			<%-	tables.each{ |table|
		tablename = table.getName
		options = table.getOptions
		options.each { |option|
			type = option["type"] 
			optionSelected = option["optionSelected"]
			if (type == "conjunction" and optionSelected == "nil")-%>
				if (name == "<%= tablename %>" && !bIsInsert)
				{
					if (filterIsSelected)
					{
						std::cout << "Filter has been already selected. only one filter is allowed for the table <%= tablename.capitalize %>" << std::endl;
						continue;	
					}
				}	
	<%-end }-%> <%- } -%>
				entity->setFilterFields (field, sInput);
				filterIsSelected = true;

			}
		}
		else
		{
			if(bIsInsert)
			{
<%-	tables.each{ |table|
		tablename = table.getName
		columns = table.getColumns -%>
					if (name == "<%= tablename %>")
					{
		<%-columns.each { |column|
			name = column["name"] 
			nullable = column["nullable"] -%>

				<%-	if (nullable == "N")-%>
						if ("<%= name %>" == field)
							throw std::runtime_error("This field cannot be empty");		
	<% end }%> 					} 
<% } %>
			}

		}
	
	}
	
}
			
void fillFieldParamsPerOption (int iAction, EntityBase* entity)
{
	bool bIsField = true;
	switch(iAction)
	{
	case eActions::INSERT:
		bIsField = true;
		fillFieldParams (entity, bIsField, true);
		
		break;

	case eActions::DELETE:
		bIsField = false;
		fillFieldParams (entity, bIsField, false);
		
		break;

	case eActions::SELECT:
		bIsField = false;
		fillFieldParams (entity, bIsField, false);	
		break;

	case eActions::UPDATE:
		// set the values for the fields to be modified
		bIsField = true;
		fillFieldParams (entity, bIsField, false);	

		//set the fields to use in the filter
		std::cout << "Set filters" << std::endl;
		//std::system("pause");
		bIsField = false;
		fillFieldParams (entity, bIsField, false);	
		
		break;

	}
		
}

void setAndExecuteAction (int iOption, Tables eTableType, EntityBase* entity, eDatabase db)
{
	displayOptions (iOption);

	fillFieldParamsPerOption (iOption, entity);

<%-counter = 0
names.each { |name| 
	if (name != "nil")
	if (counter != 0)-%>	else <% else %>	<% end %>if (eTableType == Tables::<%= name.upcase %>) {
		
		<%= name.capitalize %>Repository *<%= name %>Repository = new <%= name.capitalize %>Repository();
		<%= name.capitalize %>Logic* <%= name %> = dynamic_cast<<%= name.capitalize %>Logic*>(entity);
					
		if (<%= name %> !=nullptr) {
			executeAction(<%= name %>Repository, db, <%= name %>, iOption);
		}
		
	}
<%- end -%>
<%- counter = counter + 1} -%>		
						
}
				

eDatabase setUpDatabase (int iInput)
{
	eDatabase db;

	if (iInput == 1) {

		//connect to the MYSQL database
		db.user = "root";
		db.password = "Qc62fTLa";
		db.server = "localhost";
		db.database = "school";

	} 
	else if (iInput == 2) {
		//connect to the SQLITE database
	}
	else
	{
		throw std::runtime_error("Only MSQL and sqlite databases are allowed. Please selet option 1 or 2.");
	}

	return db;
}

Tables getEnumTable (std::string tablename)
{
	Tables table = Tables::NONE;
<%-counter = 0
names.each { |name| 
	if (name != "nil")
	if (counter != 0)-%>	else <% else %>	<% end %>if (tablename == "<%= name %>") {
		table = Tables::<%= name.upcase %>;
		
	}
<%- end -%>
<%- counter = counter + 1} -%>	
	return table;
}

EntityBase* createEntity (Tables eTableType)
{
	Factory factory;

	EntityBase *entity = factory.createEntity(eTableType);

	return entity;
}


int main ()
{
	try
	{
		//show to the user the two options to connect to the databae MYSQL or SQLite
		std::cout << "Select database:" << std::endl;
		std::cout << "1 - Database Mysql" << std::endl;
		std::cout << "2 - SQLite" << std::endl;

		std::string sInput;
		int iInput = 0;

		getline (std::cin, sInput);
		std::stringstream(sInput) >> iInput;

	 	eDatabase db = setUpDatabase (iInput);

		//ask the user to enter the table name over the actions will be applied to
		std::system("clear");
		std::cout << "Insert table name:" << std::endl;
	
		std::string sTablename;	
		getline (std::cin, sTablename);

		Tables eTableType = getEnumTable (sTablename);

		EntityBase* entity = createEntity (eTableType);
		if (entity == nullptr)
		{
			std::string msg = "Entity " + sTablename +  "doesn't exist";
			throw std::runtime_error(msg);
		}

		std::system("clear");

		//ask the user to select the action to apply to the database
		std::cout << "Menu options:" << std::endl;
		std::cout << "1 - Insert" << std::endl;
		std::cout << "2 - Delete" << std::endl;
		std::cout << "3 - Update" << std::endl;
		std::cout << "4 - Select" << std::endl;


		getline (std::cin, sInput);
		std::stringstream(sInput) >> iInput;

		setAndExecuteAction (iInput, eTableType, entity, db);

		return 0;
	}
	catch(std::exception const& e)
	{
		std::cout << "Exception: " << e.what() << "\n";
	} 

}
