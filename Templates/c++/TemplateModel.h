#ifndef <%= table.getName.upcase %>_MODEL_H
#define <%= table.getName.upcase %>_MODEL_H

#include <string>
#include <vector>

class <%= table.getName.capitalize %>Model 
{
public:
	<%= table.getName.capitalize %>Model();
	std::string getTableName();
	void setTableName(std::string);

	//setters and getters for the fields
<%- columns.each { |column|
	name = column["name"] -%>
	void set<%= name.capitalize %>Field(std::string str<%= name.capitalize %>);
	std::string get<%= name.capitalize %>Field();	
<%}%>	
	//helpers to check if the field has been modified
	void setDirty(std::string strField);
	bool isDirty(std::string strField);

<%- columns.each { |column|
	name = column["name"] -%>
	void set<%= name.capitalize %>FieldUpdated(bool bUpdated);
	bool is<%= name.capitalize %>FieldUpdated();	
<%}%>	

	//stores all the fields
	void addField(std::string strField);
	std::vector<std::string> getFields();

	//add a filter for a field
	void addFilter(std::string strField);	
	std::vector<std::string> getFilter();
	//resets the filter
	void clearFilter();


private:

<%- columns.each { |column|
	name = column["name"] -%>
	std::string m_<%= name %>;
	bool m_bIs<%= name.capitalize %>Updated;
<%}%>	

	std::vector<std::string> fields;
	std::vector<std::string> filteredFields;
	std::string m_tableName;
};

#endif //<%= table.getName.upcase %>_MODEL_H
