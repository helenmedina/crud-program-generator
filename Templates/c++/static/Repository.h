#ifndef REPOSITORY_H
#define REPOSITORY_H

#include "EntityBase.h"

template <typename T> class Repository
{
public:
	virtual void add( T entity ) = 0;
	virtual void Delete( T entity ) = 0;
	virtual void update( T entity ) = 0;
	virtual T find(T entity) = 0;
	virtual void setDatabaseContext(eDatabase db) = 0;
};


#endif //REPOSITORY_H
