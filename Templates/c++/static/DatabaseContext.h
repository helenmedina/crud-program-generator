#ifndef DATABASE_CONTEXT_H
#define DATABASE_CONTEXT_H

#include "EntityBase.h"
#include <stdlib.h>
#include <mysql_connection.h>
#include <mysql_driver.h> 
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>


class DatabaseContext
{
public:
	DatabaseContext(std::string strServer, std::string strUser, std::string strPassword, std::string db);
	~DatabaseContext();
	void addEntity(EntityBase*);
	void deleteEntity(EntityBase*);
	void updateEntity(EntityBase*);
	void find(EntityBase* entity);
	void connect(std::string);

private:

	const std::string server;
	const std::string username;
	const std::string password; 
	sql::Driver     *driver; 
	sql::Connection *dbConn; 
	sql::Statement  *stmt; 

};


#endif


