#ifndef FACTORY_H
#define FACTORY_H

#include "shared.h"
#include "EntityBase.h"

class Factory
{
public:
	EntityBase* createEntity (Tables T);
};


#endif // FACTORY_H
