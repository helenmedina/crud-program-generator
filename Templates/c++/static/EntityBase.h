#ifndef ENTITY_BASE_H
#define ENTITY_BASE_H

#include "shared.h"
#include <string>
#include <cppconn/driver.h>
#include <vector>

class EntityBase
{
public:
	virtual void insert (sql::Connection *db) = 0;
	virtual void Delete (sql::Connection *db) = 0;
	virtual void update (sql::Connection *db) = 0;
	virtual void setField (std::string field, std::string valueField) = 0;
	virtual void setFilterFields (std::string field, std::string valueField) = 0;
	virtual void showDetails () = 0;
	virtual std::string getTable() = 0;
	virtual EntityBase* find(sql::Connection *db) = 0;
	virtual std::vector<std::string> getFields() = 0;
	virtual std::vector<std::string> getFilterFields () = 0;
		
};

#endif //ENTITY_BASE_H
