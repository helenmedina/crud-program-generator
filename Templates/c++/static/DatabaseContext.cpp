#include "DatabaseContext.h"

DatabaseContext::DatabaseContext(std::string strServer, std::string strUser, std::string strPassword, std::string database) : server(strServer), username(strUser), password(strPassword)
{
	driver = nullptr; 
	dbConn = nullptr; 
	stmt = nullptr; 
	connect(database);
}

DatabaseContext::~DatabaseContext()
{
	delete stmt;
	delete dbConn;
}

void DatabaseContext::addEntity(EntityBase* entity)
{
	entity->insert(dbConn);
}

void DatabaseContext::deleteEntity(EntityBase* entity)
{
	entity->Delete(dbConn);
}

void DatabaseContext::updateEntity(EntityBase* entity)
{
	entity->update(dbConn);
}

void DatabaseContext::find(EntityBase* entity)
{
	entity->find(dbConn);
}

void DatabaseContext::connect(std::string db)
{

  /* Create a connection */
  driver = get_driver_instance();
  dbConn = driver->connect(server, username, password);
  /* Connect to the MySQL test database */
  dbConn->setSchema(db);

  stmt = dbConn->createStatement();

}



