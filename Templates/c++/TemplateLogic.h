#ifndef <%= table.getName.upcase %>_LOGIC_H
#define <%= table.getName.upcase %>_LOGIC_H

#include "EntityBase.h"
#include <cppconn/driver.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <vector>

using namespace std;


class <%= table.getName.capitalize %>Model;

class <%= table.getName.capitalize %>Logic : public EntityBase
{
public:
	<%= table.getName.capitalize %>Logic();
	~<%= table.getName.capitalize %>Logic();
	void insert (sql::Connection *db);
	void Delete (sql::Connection *db);
	void update (sql::Connection *db);

	<%= table.getName.capitalize %>Logic* find(sql::Connection *db);

<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
	<%= type %> get<%= name.capitalize %>();
	void set<%= name.capitalize %>(<%= type %>);
<%}%>

	void setField (string field, string valueField);
	void setFilterFields (string field, string valueField);
	std::vector<string> getFilterFields();
	std::vector<string> getFields();
	void showDetails();

	//filter
<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
	void setFilterBy<%= name.capitalize %>(<%= type %> <%= name %>);
<%}%>


protected:
	void execute(sql::PreparedStatement  *prep_stmt);
	std::string getFieldsToInsert();
	std::string getTable();
	void getValue(std::string strField, int i, sql::PreparedStatement  *prep_stmt);
	void bindValues(sql::PreparedStatement  *prep_stmt);
	std::string getFilter();
	void bindFilterValues(sql::PreparedStatement  *prep_stmt, int);
	std::string getColumnsUpdated();
	void updateValue(std::string strField, sql::ResultSet *res);
	std::string getValuesToBind();
	void getFilterValue(std::string strField, int i, sql::PreparedStatement  *prep_stmt);
	//filter
<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
	void setFilter<%= name.capitalize %>(<%= type %> <%= name %>);
	<%= type %> getFilter<%= name.capitalize %>();
<%}%>

private:

	std::string m_query;
	std::string m_filter;
	std::string m_auxBindingValues;

<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
	<%= type %> m_<%= name %>;
<%}%>
<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
	<%= type %> m_filter<%= name.capitalize %>;
<%}%>

	int m_countFields; 

	<%= table.getName.capitalize %>Model* m_model;

};

#endif //<%= table.getName.upcase %>_LOGIC_H
