#include "<%= table.getName.capitalize %>Logic.h"
#include "<%= table.getName.capitalize %>Model.h"
#include <cppconn/prepared_statement.h>
#include <iostream>


<%= table.getName.capitalize %>Logic::<%= table.getName.capitalize %>Logic()
{
	m_model = new <%= table.getName.capitalize %>Model();
<%- columns.each { |column|
	name = column["name"]
	type = column["type"]  
if (type == "int")-%>
	m_<%= name %> = 0;
	m_filter<%= name.capitalize %> = 0;
<%-elsif (type == "double") -%>
	m_<%= name %> = 0.0;
	m_filter<%= name.capitalize %> = 0.0;
<%-elsif (type == "string") -%>
	m_<%= name %> = "";
	m_filter<%= name.capitalize %> = "";
<%-elsif (type == "date") -%>
	_<%= name %> = "";
	m_filter<%= name.capitalize %> = "";
<%-end } -%>

	m_countFields = 0;
}

<%= table.getName.capitalize %>Logic::~<%= table.getName.capitalize %>Logic()
{
	delete m_model;
}

void <%= table.getName.capitalize %>Logic::insert (sql::Connection *con)
{
	sql::PreparedStatement  *prep_stmt;
	m_query = "";
	m_query = "INSERT INTO " + getTable() + " (";
	m_query += getFieldsToInsert();
	m_query += " ) VALUES ( " + getValuesToBind() + ")";

	//cout<< m_query <<endl;
	prep_stmt = con->prepareStatement(m_query);
	bindValues(prep_stmt);
	execute(prep_stmt);

	m_model->clearFilter();
	delete prep_stmt;
	
}
void <%= table.getName.capitalize %>Logic::Delete (sql::Connection *con)
{
	sql::PreparedStatement  *prep_stmt;
	m_query = "";
	m_query = "DELETE FROM " + getTable() + " WHERE " + getFilter();
	prep_stmt = con->prepareStatement(m_query);
	bindValues(prep_stmt);
	execute(prep_stmt);

	m_model->clearFilter();
	delete prep_stmt;
}

void <%= table.getName.capitalize %>Logic::update (sql::Connection *con)
{
	sql::PreparedStatement  *prep_stmt;
	m_query = "";
	m_query = "UPDATE " + getTable() + " SET " + getColumnsUpdated() + " WHERE " + getFilter();
	//cout<< "query: " << m_query <<endl;
	prep_stmt = con->prepareStatement(m_query);
	bindValues(prep_stmt);
	execute(prep_stmt);
	m_model->clearFilter();
	delete prep_stmt;
}

<%= table.getName.capitalize %>Logic* <%= table.getName.capitalize %>Logic::find(sql::Connection *con)
{
	sql::PreparedStatement  *prep_stmt;
	sql::ResultSet *res;
	m_query = "";
	m_query = "SELECT * FROM " + getTable() + " WHERE " + getFilter();
	//cout<< m_query <<endl;
	prep_stmt = con->prepareStatement(m_query);
	bindValues(prep_stmt);
	res = prep_stmt->executeQuery();
	if (res->next())
	{
		vector<string> fields = m_model->getFields();

		for (unsigned int i = 0; i < fields.size(); i++)
		{
			updateValue(fields[i], res);
		}

	}
	m_model->clearFilter();
	return this;
}

void <%= table.getName.capitalize %>Logic::execute(sql::PreparedStatement  *prep_stmt)
{
<%- options.each { |option|
	type = option["type"] 
	optionSelected = option["optionSelected"]
		if (type == "resultQueryExecution") -%>
	<%- if (optionSelected == "resultQuery") -%>
	int res = prep_stmt->executeUpdate();
	if (res > 0)
		cout << "Succes." <<endl;
	else
		cout << "Failed." <<endl;
	<%- elsif (optionSelected == "numberRowsExecuted") -%>
	int rows = prep_stmt->executeUpdate();
	cout<< rows << " affected." <<endl;
	<%- else -%>
	prep_stmt->execute();
<%-end -%>
<%-end } -%>
}

void <%= table.getName.capitalize %>Logic::showDetails()
{
	system("clear");

	string sInput;
	vector<string> fields = m_model->getFields();
	
	for (unsigned int i = 0; i < fields.size(); i++)
	{
		string field = fields[i];
		<%- counter = 0
			columns.each { |column|
			name = column["name"] 
			if (counter == 0)-%>
		if (field == m_model->get<%= name.capitalize %>Field())
			cout << field << ": " << get<%= name.capitalize %>() << endl;
			<%-else -%>
		else if (field == m_model->get<%= name.capitalize %>Field())
			cout << field << ": " << get<%= name.capitalize %>() << endl;
		<%-end 
		counter = counter +1} -%>

	}

}

void <%= table.getName.capitalize %>Logic::setField (string field, string valueField)
{

<%- counter = 0
	columns.each { |column|
	name = column["name"] 
	type = column["type"] 
	if (counter != 0)-%>	else <% else %>	<% end %>if (field == m_model->get<%= name.capitalize %>Field()) {
		<%-if (type == "int")-%>	
	<%= type%> <%= name%> = atoi(valueField.c_str());
	set<%= name.capitalize %>(<%= name %>);			
		<%-elsif (type == "double") -%>
	<%= type%> <%= name%> = atof(valueField.c_str());
	set<%= name.capitalize %>(<%= name %>);	
		<%-elsif (type == "string") -%>
	set<%= name.capitalize %> (valueField);
		<%-elsif (type == "date") -%>
	set<%= name.capitalize %> (valueField);			
		<%-end -%>
	} 
<%- counter = counter +1} -%>
	
}

void <%= table.getName.capitalize %>Logic::setFilterFields (string field, string valueField)
{

<%- counter = 0
	columns.each { |column|
	name = column["name"] 
	type = column["type"] 
	if (counter != 0)-%>	else <% else %>	<% end %>if (field == m_model->get<%= name.capitalize %>Field()) {
		<%-if (type == "int")-%>	
	<%= type%> <%= name%> = atoi(valueField.c_str());
	setFilterBy<%= name.capitalize %>(<%= name %>);			
		<%-elsif (type == "double") -%>
	<%= type%> <%= name%> = atof(valueField.c_str());
	setFilterBy<%= name.capitalize %>(<%= name %>);	
		<%-elsif (type == "string") -%>
	setFilterBy<%= name.capitalize %> (valueField);
		<%-elsif (type == "date") -%>
	setFilterBy<%= name.capitalize %> (valueField);			
		<%-end -%>
	} 
<%- counter = counter +1} -%>

}

vector<string> <%= table.getName.capitalize %>Logic::getFields()
{
	return m_model->getFields();
}

vector<string> <%= table.getName.capitalize %>Logic::getFilterFields()
{
	return m_model->getFilter();
}



void <%= table.getName.capitalize %>Logic::bindValues(sql::PreparedStatement  *prep_stmt)
{
	vector<string> fields = m_model->getFields();

	int counter = 1;
	for (unsigned int i = 0; i < fields.size(); i++)
	{

		if (m_model->isDirty(fields[i]))
		{			
			getValue(fields[i], counter, prep_stmt);
			counter ++;
		}
			
	}
	bindFilterValues(prep_stmt, counter);
	
}

void <%= table.getName.capitalize %>Logic::bindFilterValues(sql::PreparedStatement  *prep_stmt, int counter)
{
	vector<string> fields = m_model->getFilter();

	for (unsigned int i = 0; i < fields.size(); i++)
	{		
		getFilterValue(fields[i], counter, prep_stmt);
		counter++;
	}
	
}

<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
void <%= table.getName.capitalize %>Logic::setFilterBy<%= name.capitalize %>(<%= type %> <%= name %>)
{		
	setFilter<%= name.capitalize %>(<%= name %>);
	m_model->addFilter(m_model->get<%= name.capitalize %>Field());
}		
<%}%>


string <%= table.getName.capitalize %>Logic::getFilter()
{
	vector<string> fields = m_model->getFilter();
	m_filter = "";

	for (unsigned int i = 0; i < fields.size(); i++)
	{
		if (i == 0)
			m_filter += fields[i] + " = ?";
<%- options.each { |option|
	type = option["type"] 
	optionSelected = option["optionSelected"]
		if (type == "conjunction" and optionSelected != "nil")-%>
		else
			m_filter += " <%= optionSelected.upcase %> " + fields[i] + " = ?";	
<%-end } -%>
		
	}
	return m_filter;
}

string <%= table.getName.capitalize %>Logic::getTable()
{
	return m_model->getTableName();
}

<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
void <%= table.getName.capitalize %>Logic::set<%= name.capitalize %>(<%= type %> <%= name %>)
{
	m_model->setDirty("<%= name %>");
	this->m_<%= name %> = <%= name %>;
}

<%= type %> <%= table.getName.capitalize %>Logic::get<%= name.capitalize %>()
{
	return m_<%= name %>;
}	
<%}%>

<%- columns.each { |column|
	name = column["name"]
	type = column["type"] -%>
void <%= table.getName.capitalize %>Logic::setFilter<%= name.capitalize %>(<%= type %> <%= name %>)
{

	this->m_filter<%= name.capitalize %> = <%= name %>;
}

<%= type %> <%= table.getName.capitalize %>Logic::getFilter<%= name.capitalize %>()
{
	return m_filter<%= name.capitalize %>;
}	
<%}%>


string <%= table.getName.capitalize %>Logic::getFieldsToInsert()
{
	string strFields;
	vector<string> fields = m_model->getFields();
	int counter = 0;

	for (unsigned int i = 0; i < fields.size(); i++)
	{
		m_countFields++;
		if (m_model->isDirty(fields[i]))
		{
			if(counter != 0)
			{
				strFields += ", ";
				strFields += fields[i];
				m_auxBindingValues += ", ";
				m_auxBindingValues += "?";
			}
			else
			{
				strFields += fields[i];
				m_auxBindingValues += "?";
			}
			counter++;
		}
		
	}
	
	return strFields;
}
string <%= table.getName.capitalize %>Logic::getValuesToBind()
{
	return m_auxBindingValues;
}

void <%= table.getName.capitalize %>Logic::getValue(string strField, int i, sql::PreparedStatement  *prep_stmt)
{

<%- counter = 0
	columns.each { |column|
	name = column["name"] 
	type = column["type"] 
	if (counter != 0)-%>	else <% else %>	<% end %>if (strField == m_model->get<%= name.capitalize %>Field()) {
		<%-if (type == "int")-%>	
		prep_stmt->setInt(i, get<%= name.capitalize %>());			
		<%-elsif (type == "double") -%>
		prep_stmt->setDouble(i, get<%= name.capitalize %>());	
		<%-elsif (type == "string") -%>
		prep_stmt->setString(i, get<%= name.capitalize %>());
		<%-elsif (type == "date") -%>
		prep_stmt->setString(i, get<%= name.capitalize %>());		
		<%-end -%>
	} 
<%- counter = counter +1} -%>

}

void <%= table.getName.capitalize %>Logic::getFilterValue(string strField, int i, sql::PreparedStatement  *prep_stmt)
{

<%- counter = 0
	columns.each { |column|
	name = column["name"] 
	type = column["type"] 
	if (counter != 0)-%>	else <% else %>	<% end %>if (strField == m_model->get<%= name.capitalize %>Field()){
		<%-if (type == "int")-%>	
		prep_stmt->setInt(i, getFilter<%= name.capitalize %>());			
		<%-elsif (type == "double") -%>
		prep_stmt->setDouble(i, getFilter<%= name.capitalize %>());	
		<%-elsif (type == "string") -%>
		prep_stmt->setString(i, getFilter<%= name.capitalize %>());
		<%-elsif (type == "date") -%>
		prep_stmt->setString(i, getFilter<%= name.capitalize %>());		
		<%-end -%>
	} 
<%- counter = counter +1} -%>

}

void <%= table.getName.capitalize %>Logic::updateValue(string strField, sql::ResultSet *res)
{
<%- counter = 0
	columns.each { |column|
	name = column["name"] 
	type = column["type"] 
	if (counter != 0)-%>	else <% else %>	<% end %>if (strField == m_model->get<%= name.capitalize %>Field()){
		<%-if (type == "int")-%>	
		this->set<%= name.capitalize %>(res->getInt(m_model->get<%= name.capitalize %>Field()));			
		<%-elsif (type == "double") -%>
		this->set<%= name.capitalize %>(res->getDouble(m_model->get<%= name.capitalize %>Field()));
		<%-elsif (type == "string") -%>
		this->set<%= name.capitalize %>(res->getString(m_model->get<%= name.capitalize %>Field()));
		<%-elsif (type == "date") -%>
		this->set<%= name.capitalize %>(res->getString(m_model->get<%= name.capitalize %>Field()));		
		<%-end -%>
	} 
<%- counter = counter +1} -%>

}

string <%= table.getName.capitalize %>Logic::getColumnsUpdated()
{
	string str;
	vector<string> fields = m_model->getFields();
	int count = 0;

	for (unsigned int i = 0; i < fields.size(); i++)
	{

		if (m_model->isDirty(fields[i]))
		{
			
			if (count == 0)
				str += fields[i] + " = ?";
			else
				str += "," +fields[i] + " = ?";
			count++;

		}
			
	}
	return str;
}


