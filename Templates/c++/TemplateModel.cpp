#include "<%= table.getName.capitalize %>Model.h"

<%= table.getName.capitalize %>Model::<%= table.getName.capitalize %>Model()
{
	fields.clear();

	m_tableName = "<%= table.getName %>";
<%- columns.each { |column|
	name = column["name"] -%>
	set<%= name.capitalize %>Field("<%= name %>");
<%}%>	
}

std::string <%= table.getName.capitalize %>Model::getTableName()
{
	return m_tableName;
}

void <%= table.getName.capitalize %>Model::setTableName(std::string name)
{
	this->m_tableName = name;
}

<%- columns.each { |column|
	name = column["name"] -%>
void <%= table.getName.capitalize %>Model::set<%= name.capitalize %>Field(std::string str<%= name.capitalize %>)
{
	this->m_<%= name %> = str<%= name.capitalize %>;
	set<%= name.capitalize %>FieldUpdated(false);
	addField(this->m_<%= name %>);
}

std::string <%= table.getName.capitalize %>Model::get<%= name.capitalize %>Field()
{
	return this->m_<%= name %>;
}
<%}%>	

<%- columns.each { |column|
	name = column["name"] -%>
void <%= table.getName.capitalize %>Model::set<%= name.capitalize %>FieldUpdated(bool bUpdated)
{
	this->m_bIs<%= name.capitalize %>Updated = bUpdated;
}

bool <%= table.getName.capitalize %>Model::is<%= name.capitalize %>FieldUpdated()
{
	return this->m_bIs<%= name.capitalize %>Updated;
}
<%}%>	

void <%= table.getName.capitalize %>Model::addField(std::string strField)
{
	fields.push_back(strField);
}

std::vector<std::string> <%= table.getName.capitalize %>Model::getFields()
{
	return fields;
}

void <%= table.getName.capitalize %>Model::setDirty(std::string strField)
{
<%- counter = 0
	columns.each { |column|
	name = column["name"] 
	if (counter == 0)-%>
	if (strField == get<%= name.capitalize %>Field())
		set<%= name.capitalize %>FieldUpdated(true);
	<%-else -%>
	else if (strField == get<%= name.capitalize %>Field())
		set<%= name.capitalize %>FieldUpdated(true);
<%-end 
counter = counter +1} -%>	
}

bool <%= table.getName.capitalize %>Model::isDirty(std::string strField)
{
<%- counter = 0
	columns.each { |column|
	name = column["name"] 
	if (counter == 0)-%>
	if (strField == get<%= name.capitalize %>Field())
		return is<%= name.capitalize %>FieldUpdated();
	<%-else -%>
	else if (strField == get<%= name.capitalize %>Field())
		return is<%= name.capitalize %>FieldUpdated();
<%-end 
counter = counter +1} -%>	
}

void <%= table.getName.capitalize %>Model::addFilter(std::string strField)
{
	filteredFields.push_back(strField);
}

std::vector<std::string> <%= table.getName.capitalize %>Model::getFilter()
{
	return filteredFields;
}
void <%= table.getName.capitalize %>Model::clearFilter()
{
	filteredFields.clear();
}


