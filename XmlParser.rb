require 'rexml/document'
require "rexml/parseexception"
require_relative 'Table.rb'


class String
  def numeric?
    true if Float self rescue false
  end
end
class XmlParser
	
	def initialize
		@arrayTables = []
		@arrayTableNames = []
	end
		
	def parse(doc)
		print "Parsing tables\n"
		doc.root.each_element( "table" ) { |tableElement|
			table_name = tableElement.attributes[ "name" ]
			print "#{table_name}  \n"
			@table = Table.new(table_name)
			#Get columns information
			setColumns(@table, tableElement)
			#puts @table.getColumns
			setOptions(@table, tableElement)
			#puts @table.getOptions
			@arrayTableNames << table_name
			@arrayTables << @table
		
		}

	end
	
	def getTables
		return @arrayTables

	end

	def getTableNames
		return @arrayTableNames

	end

	def setColumns(table, columns)
		REXML::XPath.match(columns, './/columns/column').map do |column|			
			type = column.attributes[ "type" ]
			isNull = column.attributes[ "nullable" ]
			if column.text.nil? || column.text.empty?
				raise Exception , "column cannot be empty."
			else
				columnName = column.text.strip
			end
			type = getType(type, columnName)	
			table.insertColum({name: columnName, type: type, nullable: isNull})
		end	
	end

	def setOptions(table, options)
		
		REXML::XPath.match(options, './/options/option').map do |option|			
			typeSelected = option.attributes[ "type" ]

			if option.text.nil? || option.text.empty?		
				optionSelected = "nil"
					
			else
				optionSelected = option.text.strip	
			end
				
			table.insertOptions({type: typeSelected, optionSelected: optionSelected})
		end
	end
	
	def getTable()
	
		return @table
	
	end
	
	def getType(type, column)
			print "#{column} #{type}  \n"
			#Get the correspondent data type for the QT creator IDE
			if(type == nil || type.empty?)
				raise Exception , "Type column \"#{column}\" is null."
			else
				case type.downcase
				when "decimal"
					type = 'double'
				when "int"
					type = 'int'
				when "varchar"
					type = 'string'
				when "date"
					type = 'string'
				else
					raise Exception , "Type \"#{type}\" unknown."
				end
			end
			
		return type
		
	end
	
end


