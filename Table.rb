class Table

	SColumns = Struct.new(:name, :type, :nullable)
	SOptions = Struct.new(:type, :optionSelected)

	attr_accessor :name

	def initialize(name)

		@name = name
		@arrayColumn = []
		@arrayOptions = []

	end
	#add a new column in the table
	def insertColum(column)

		@column = SColumns.new(column[:name], column[:type], column[:nullable]) 
		@arrayColumn.push(@column)

	end

	#insert options config in the xml config
	def insertOptions(option)

		@option = SOptions.new(option[:type], option[:optionSelected]) 
		@arrayOptions.push(@option)

	end
	
	def getColumns

	 return @arrayColumn

	end

	def getOptions

	 return @arrayOptions

	end

	def getName

	 return @name

	end
	
end



