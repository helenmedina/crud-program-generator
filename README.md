# README #

CRUD generator.

### What is this repository for? ###

##### Quick summary #####

Generator generates a command line program to connect to Database MySQL and apply the following functions:

 -Insert records
 
 -Delete records
 
 -Update records
 
 -Select records

### How do I get set up? ###

##### Summary of set up

Generate code files for C++ and C# to make create, read, update and delete operations

Below you can find the pattern design of the program:

##### ![52x37, 20%](images/pattern.png)



##### Configuration #####
Configuration is handle in input.xml file

##### Dependencies #####
Ruby

##### How to run tests #####
To create the program for C#

- Create a new project AppDB in the cs folder.

- Add the package "Mysql.Data"

- Add the reference "Mysql.Data"

- Build the project

To create the program for C++

- Run make in the source folder
