require_relative 'XmlParser.rb'
require 'erb'

#This class defines the programming lenguages supported by the system
class Lenguage
  CPP=1
  CS=2 
end

#Global variable lenguage to select the programming lenguage to generate
$lenguage = Lenguage::CS

def createSharedCPP(names)

	#create the output shared file for c++ from the template information
	erb = ERB.new( File.open( "./Templates/c++/TemplateShared.h" ).read, nil, '-' )
	@SharedFile = erb.result( binding )
	File.open( "./output/c++/headers/shared.h", "w" ).write( @SharedFile )

	print "Creating shared.h file\n"

end

def createSharedCS(names)

	#create the output shared file for c# lenguage from the template information

	erb = ERB.new( File.open( "./Templates/c#/TemplateShared.cs" ).read, nil, '-' )
	@SharedFile = erb.result( binding )
	File.open( "./output/cs/AppDB/AppDB/shared.cs", "w" ).write( @SharedFile )

	print "Creating shared.cs file\n"

end


def createFactoryCS(names)

	#create the output factory for c# lenguage files binding the template information
	erb = ERB.new( File.open( "./Templates/c#/TemplateFactory.cs" ).read, nil, '-' )
	@FactoryFile = erb.result( binding )
	File.open( "./output/cs/AppDB/AppDB/Factory.cs", "w" ).write( @FactoryFile )

	print "Creating Factory.cs file\n"

end

def createFactoryCPP(names)

	#create the output factory for c++ lenguage files binding the template information
	erb = ERB.new( File.open( "./Templates/c++/TemplateFactory.cpp" ).read, nil, '-' )
	@FactoryFile = erb.result( binding )
	File.open( "./output/c++/source/Factory.cpp", "w" ).write( @FactoryFile )

	print "Creating Factory.cpp file\n"

end

def createFilesForRepositoryCPP(names)

	#create the output files for c+ lenguage files binding the template information
	#creates repository.h 
	names.each{ |name|
		erb = ERB.new( File.open( "./Templates/c++/TemplateRepository.h" ).read, nil, '-' )
		@RepositoryHeaderFile = erb.result( binding )
		File.open( "./output/c++/headers/#{name.capitalize}Repository.h", "w" ).write( @RepositoryHeaderFile )
		print "Creating #{name.capitalize}Repository.h file\n"
	}

	#creates repository.cpp
	names.each{ |name|
		erb = ERB.new( File.open( "./Templates/c++/TemplateRepository.cpp" ).read, nil, '-' )
		@RepositoryCPPFile = erb.result( binding )
		File.open( "./output/c++/source/#{name.capitalize}Repository.cpp", "w" ).write( @RepositoryCPPFile )
		print "Creating #{name.capitalize}Repository.cpp file\n"
	}

end

def createRepositoryCS(names)

	#create the output files for c# lenguage files binding the template information
	#creates repository.h 
	
	names.each{ |name|
		
		erb = ERB.new( File.open( "./Templates/c#/TemplateRepository.cs" ).read, nil, '-' )
		@RepositoryFile = erb.result( binding )
		File.open( "./output/cs/AppDB/AppDB/#{name.capitalize}Repository.cs", "w" ).write( @RepositoryFile )
		print "Creating #{name.capitalize}Repository.cs file\n"

	}

end

def createFilesForModelCPP(tables)

	#create the output files for c+ lenguage files binding the template information
	#creates Model.h 
	tables.each{ |table|
		columns = table.getColumns  
		erb = ERB.new( File.open( "./Templates/c++/TemplateModel.h" ).read, nil, '-' )
		@ModelHeaderFile = erb.result( binding )
		File.open( "./output/c++/headers/#{table.getName.capitalize}Model.h", "w" ).write( @ModelHeaderFile )
		print "Creating #{table.getName.capitalize}Model.h file\n"
	}

	#creates Model.cpp
	tables.each{ |table|
		columns = table.getColumns  
		erb = ERB.new( File.open( "./Templates/c++/TemplateModel.cpp" ).read, nil, '-' )
		@ModelCPPFile = erb.result( binding )
		File.open( "./output/c++/source/#{table.getName.capitalize}Model.cpp", "w" ).write( @ModelCPPFile )
		print "Creating #{table.getName.capitalize}Model.cpp file\n"
	}

end

def createModelCS(tables)

	#create the output files for c# lenguage files binding the template information
	#creates Model.cs
	tables.each{ |table|
		columns = table.getColumns  
		erb = ERB.new( File.open( "./Templates/c#/TemplateModel.cs" ).read, nil, '-' )
		@ModelFile = erb.result( binding )
		File.open( "./output/cs/AppDB/AppDB/#{table.getName.capitalize}Model.cs", "w" ).write( @ModelFile )
		print "Creating #{table.getName.capitalize}Model.cs file\n"
	}

end

def createLogicCS(tables)

	#create the output files for c# lenguage files binding the template information
	#creates Logic.cs
	tables.each{ |table|
		columns = table.getColumns  
		options = table.getOptions

		erb = ERB.new( File.open( "./Templates/c#/TemplateLogic.cs" ).read, nil, '-' )
		@LogicFile = erb.result( binding )
		File.open( "./output/cs/AppDB/AppDB/#{table.getName.capitalize}Logic.cs", "w" ).write( @LogicFile )
		print "Creating #{table.getName.capitalize}Logic.cs file\n"
	}

end

def createFilesForLogicCPP(tables)

	#create the output files for c+ lenguage files binding the template information
	#creates Logic.h 
	tables.each{ |table|
		columns = table.getColumns  

		erb = ERB.new( File.open( "./Templates/c++/TemplateLogic.h" ).read, nil, '-' )
		@LogicHeaderFile = erb.result( binding )
		File.open( "./output/c++/headers/#{table.getName.capitalize}Logic.h", "w" ).write( @LogicHeaderFile )
		print "Creating #{table.getName.capitalize}Logic.h file\n"
	}

	#creates Logic.cpp
	tables.each{ |table|
		columns = table.getColumns 
		options = table.getOptions
 		
		erb = ERB.new( File.open( "./Templates/c++/TemplateLogic.cpp" ).read, nil, '-' )
		@LogicCPPFile = erb.result( binding )
		File.open( "./output/c++/source/#{table.getName.capitalize}Logic.cpp", "w" ).write( @LogicCPPFile )
		print "Creating #{table.getName.capitalize}Logic.cpp file\n"
	}

end

def createMainCPP(names, tables)

	#create the output Main for c++ lenguage files binding the template information
	erb = ERB.new( File.open( "./Templates/c++/TemplateMain.cpp" ).read, nil, '-' )
	@MainFile = erb.result( binding )
	File.open( "./output/c++/source/main.cpp", "w" ).write( @MainFile )

	print "Creating main.cpp file\n"

end

def createProgramCS(names, tables)
	#create the output program.cs for c# lenguage files binding the template information
	erb = ERB.new( File.open( "./Templates/c#/TemplateProgram.cs" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	File.open( "./output/cs/AppDB/AppDB/Program.cs", "w" ).write( @ProgramFile )

	print "Creating program.cs file\n"

end

def createMakefile(names)

	#This file generated build the program for c++ environment
	erb = ERB.new( File.open( "./Templates/c++/TemplateMakefile" ).read, nil, '-' )
	@MakefileFile = erb.result( binding )
	File.open( "./output/c++/source/Makefile", "w" ).write( @MakefileFile )

	print "Creating Makefile file\n"

end

begin

	fileName = 'input.xml'
	doc = REXML::Document.new( File.open( "#{fileName}" ) )
	
	#parse the input file
	parser = XmlParser.new
	parser.parse(doc)
	tables =  parser.getTables

	case $lenguage
	when 1
		createSharedCPP(parser.getTableNames)
		createFactoryCPP(parser.getTableNames)
		createFilesForRepositoryCPP(parser.getTableNames)
		createFilesForModelCPP(tables)
		createFilesForLogicCPP(tables)
		createMainCPP(parser.getTableNames, tables)
		createMakefile(parser.getTableNames)
	when 2
		createSharedCS(parser.getTableNames)
		createFactoryCS(parser.getTableNames)
		createRepositoryCS(parser.getTableNames)
		createModelCS(tables)
		createLogicCS(tables)
		createProgramCS(parser.getTableNames, tables)
	else
		raise Exception , "lenguage not supported."
	end
	
rescue REXML::ParseException => e
	puts "Failed when parsing document: #{e.message}"
rescue RuntimeError => e
	puts "Failed: #{e.message}"
rescue 
	puts "Failed: #{$!.message}" 
end

